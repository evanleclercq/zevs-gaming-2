<?php

    session_start();
    include_once("./modules/top.php");

    $_SESSION['page'] = "review.php";

    // Will not let you review / submit order until payment details are set.

    // if (!isset($_POST['payment'])) {
    //     header ("location: ./checkout.php");
    //     exit;
    // }


    if (isset($_POST['finish'])) {

        $_SESSION['user'] = array (
                        'fName' => $_POST['fname'],
                        'sName' => $_POST['sname'],
                        'email' => $_POST['email'],
                        'phone' => $_POST['phone'],
                        'address' => $_POST['address'],
                        'suburb' => $_POST['suburb'],
                        'postcode' => $_POST['postcode'],
                        );

        $_SESSION['payment'] = array (
                        'cardName' => $_POST['cardname'],
                        'cardType' => $_POST['cardtype'],
                        'cardNo' => $_POST['cardno'],
                        'cardExp' => $_POST['month'] . '/' . $_POST['year'],
                        'cardCCV' => $_POST['ccv'],
                        );

        $cardNo = $_SESSION['payment']['cardNo'];

        $cardStart = substr($cardNo, 0, 2);
        $cardEnd = substr($cardNo, -4);

        $_SESSION['payment']['cardNo'] = $cardStart . "XX XXXX XXXX " . $cardEnd;

    }
    

?>

    <title>Review Order</title>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

            <div id = "reviewOrder">

                <?php

                    // echo ("<pre>");
                    // print_r ($_SESSION['user']);
                    // echo ("</pre>");

                    // echo ("<br /><br />");

                    // echo ("<pre>");
                    // print_r ($_SESSION['payment']);
                    // echo ("</pre>");

                    // echo ("<pre>");
                    // print_r ($_SESSION['cart']);
                    // echo ("</pre>");

                    echo ("<table id = 'info'><tr><th>Personal Information</th><th>Payment Information</th></tr><tr><td><br />");


                    if (isset($_SESSION['user'])) {

                        echo ("<b>Name: </b><br />" . $_SESSION['user']['fName'] . " " . $_SESSION['user']['sName']);
                        echo ("<br /><br />");
                        echo ("<b>Email: </b><br />" . $_SESSION['user']['email']);
                        echo ("<br /><br />");
                        echo ("<b>Phone: </b><br />" . $_SESSION['user']['phone']);
                        echo ("<br /><br />");
                        echo ("<b>Address: </b><br />" . $_SESSION['user']['address'] . ",<br />" . $_SESSION['user']['suburb'] . " " . $_SESSION['user']['postcode']);
                        echo ("<br /><br />");


                    }

                    echo ("</td><td><br />");

                    if (isset($_SESSION['payment'])) {

                        echo ("<b>Card Name: </b><br />" . $_SESSION['payment']['cardName']);
                        echo ("<br /><br />");
                        echo ("<b>Card Type: </b><br />" . $_SESSION['payment']['cardType']);
                        echo ("<br /><br />");
                        echo ("<b>Card No: </b><br />" . $_SESSION['payment']['cardNo']);
                        echo ("<br /><br />");
                        echo ("<b>Expiry: </b><br />" . $_SESSION['payment']['cardExp']);
                        echo ("<br /><br />");

                    }


                    echo ("</td></tr></table>");


                    if (isset($_SESSION['cart'])) {

                        $totalCost = 0;
                        $totalCount = 0;

                        echo ("<table id = 'cart1'>");
                        echo ("<tr><th>Product ID</th><th>Product Title</th><th>Quantity</th><th>Cost</th><th>SubTotal</th></tr>");

                        for ($i = 0; $i < count($_SESSION['cart']); $i++) {

                            switch ($i) {

                                case (0):
                                    $prt = "witcher3";
                                    break;
                                case (1):
                                    $prt = "batmanAK";
                                    break;
                                case (2):
                                    $prt = "bfh";
                                    break;
                                case (3):
                                    $prt = "banished";
                                    break;
                                case (4):
                                    $prt = "cities";
                                    break;
                                case (5):
                                    $prt = "metroLL";
                                    break;
                                case (6):
                                    $prt = "mgsv";
                                    break;
                                case (7):
                                    $prt = "projCars";
                                    break;

                            }

                            if (isset($_SESSION['cart'][$prt]['prodID'])) {

                                $prodID = $_SESSION['cart'][$prt]['prodID'];
                                $prodTitle = $_SESSION['cart'][$prt]['prodName'];
                                $prodCount = $_SESSION['cart'][$prt]['prodCount'];
                                $prodCost = $_SESSION['cart'][$prt]['prodCost'];

                                $subTotal = $prodCount * $prodCost;

                                $totalCost += $subTotal;
                                $totalCount += $prodCount;


                                if ($prodCount >= 1) {

                                    echo ("<tr><td>$prodID</td><td>$prodTitle</td><td>$prodCount</td><td>$$prodCost</td><td>$$subTotal</td></tr>");

                                }
                            }

                        }

                        echo ("</table>");

                        echo ("<table id = 'cart2'>");
                        echo ("<tr><th>Total Quantity</th><th>Total Cost</th></tr>");
                        echo ("<tr><td>$totalCount</td><td>$$totalCost</td></tr>");
                        echo ("</table>");


                        $_SESSION['cart']['total'] = array (
                                                    'totalCost' => $totalCost,
                                                    'totalCount' => $totalCount
                                                    );
                    }   

                    ?>

                    <div id = 'buttons'>

                        <form action = './orderComplete.php' method = 'post'>
                        <input id = 'submit' type = 'submit' name = 'complete' value = 'Submit Order'>
                        </form>

                        <form action = './checkout.php' method = 'post'>
                        <input id = 'submit' type = 'submit' name = 'return' value = 'Return to Checkout'>
                        </form>           

                    </div>

            </div> <!--END REVIEW ORDER-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once("./modules/end.php");
    // include_once("/home/eh1/e54061/public_html/wp/debug.php");

?>
