function checkPage (var page) {

    switch (page) {
        case "Home":
                document.getElementById(home).className = "active";
                break;
        case "Contact":
                document.getElementById(contact).className = "active";
                break;
        case "Games":
                document.getElementById(games).className = "active";
                break;
        case "Privacy":
                document.getElementById(privacy).className = "active";
                break;
        case "History":
                document.getElementById(history).className = "active";
                break;
        default:
                document.getElementById(home).className = "active";
                break;

}