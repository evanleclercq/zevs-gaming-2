<?php

    session_start();

    $_SESSION['page'] = "cart.php";


    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    } else {
        $id = "";
    }

    // $remove = "<form action = './cart.php?remove=$id' method = 'post'><input id = 'submit' type = 'submit' value = 'REMOVE' name = 'remove'/></form>";


    if (isset($_POST['add'])) {

            if (!isset($_SESSION['cart'])){
                $_SESSION['cart'] = array (
                                'witcher3' => array(),
                                'batmanAK' => array(),
                                'bfh' => array(),
                                'banished' => array(),
                                'cities' => array(),
                                'mgsv' => array(),
                                'metroLL' => array(),
                                'projCars' => array()
                                );

    }

        if (isset($_SESSION['cart'][$id]['prodCount'])) {

            $_SESSION['cart'][$id]['prodCount'] += $_POST['quantity'];

        } else {

            $_SESSION['cart'][$id] = array(
                                        'prodID' => $_POST['id'],
                                        'prodName' => $_POST['title'],
                                        'prodCount' => $_POST['quantity'],
                                        'prodCost' => $_POST['cost']
                                        );
        }

    }

    // echo ("<pre>");
    // print_r ($_SESSION['cart']);
    // echo ("</pre>");


    if (isset($_POST['empty'])) {
        unset($_SESSION['cart']);
    }

    if (isset($_POST['remove'])) {
        unset ($_SESSION['cart'][$_GET['remove']]);
    }

    include_once("./modules/top.php");

?>

        <title>Shopping Cart</title>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

            <div id = "shoppingCart">

<?php

    if (isset($_SESSION['cart'])) {

        $totalCost = 0;
        $totalCount = 0;

        echo ("<table id = 'cart1'>");
        echo ("<tr><th>Product ID</th><th>Product Title</th><th>Quantity</th><th>Cost</th><th>SubTotal</th></tr>");

        for ($i = 0; $i < count($_SESSION['cart']); $i++) {

            switch ($i) {

                case (0):
                    $prt = "witcher3";
                    break;
                case (1):
                    $prt = "batmanAK";
                    break;
                case (2):
                    $prt = "bfh";
                    break;
                case (3):
                    $prt = "banished";
                    break;
                case (4):
                    $prt = "cities";
                    break;
                case (5):
                    $prt = "metroLL";
                    break;
                case (6):
                    $prt = "mgsv";
                    break;
                case (7):
                    $prt = "projCars";
                    break;

            }

            if (isset($_SESSION['cart'][$prt]['prodID'])) {

                $prodID = $_SESSION['cart'][$prt]['prodID'];
                $prodTitle = $_SESSION['cart'][$prt]['prodName'];
                $prodCount = $_SESSION['cart'][$prt]['prodCount'];
                $prodCost = $_SESSION['cart'][$prt]['prodCost'];


                $subTotal = $prodCount * $prodCost;

                $totalCost += $subTotal;
                $totalCount += $prodCount;


                if ($prodCount >= 1) {

                    echo ("<tr><td>$prodID</td><td>$prodTitle</td><td>$prodCount</td><td>$$prodCost</td><td>$$subTotal</td></tr>");

                }
            }

        }

        echo ("</table>");

        echo ("<table id = 'cart2'>");
        echo ("<tr><th>Total Quantity</th><th>Total Cost</th></tr>");
        echo ("<tr><td>$totalCount</td><td>$$totalCost</td></tr>");
        echo ("</table>");

        echo ("<form action = './checkout.php' method = 'post'><input id = 'submit' type = 'submit' value = 'CHECKOUT' name = 'checkout'/></form> ");
        echo ("<form action = './cart.php' method = 'post'><input id = 'submit' type = 'submit' value = 'EMPTY' name = 'empty'/></form> ");


        $_SESSION['cart']['total'] = array (
                                    'totalCost' => $totalCost,
                                    'totalCount' => $totalCount
                                    );


    } else {
        echo ("<span style ='color:red;font-size:16px;font-align:center;font-weight:bold;'>There are no items currently in your cart</span>");
    }

?>

            </div> <!--END SHOPPING CART-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once ("./modules/end.php");

?>