<?php

    session_start();

    $_SESSION['page'] = "sitemap.php";

    include_once("./modules/top.php");

    $dir = scandir("./");

    // echo ("<pre>");
    // print_r($dir);
    // echo ("</pre>");

    $gameDir = scandir ("./games");

    // echo ("<pre>");
    // print_r($gameDir);
    // echo ("</pre>");


?>

    <title>Login</title>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

        <div id = "sitemap">

            <?php

                echo ("<a href = './$dir[12]'>Home</a><br />");
                echo ("<a href = './$dir[10]'>Games</a><br />");
                echo ("&nbsp &nbsp &nbsp<a href = './$dir[8]?game=$gameDir[9]'>Witcher 3 Wild Hunt</a><br />");
                echo ("&nbsp &nbsp &nbsp<a href = './$dir[8]?game=$gameDir[3]'>Batman Arkham Knight</a><br />");
                echo ("&nbsp &nbsp &nbsp<a href = './$dir[8]?game=$gameDir[4]'>Battlefield Hardline</a><br />");
                echo ("&nbsp &nbsp &nbsp<a href = './$dir[8]?game=$gameDir[2]'>Banished</a><br />");
                echo ("&nbsp &nbsp &nbsp<a href = './$dir[8]?game=$gameDir[5]'>Cities Skylines</a><br />");
                echo ("&nbsp &nbsp &nbsp<a href = './$dir[8]?game=$gameDir[6]'>Metro Last Light</a><br />");
                echo ("&nbsp &nbsp &nbsp<a href = './$dir[8]?game=$gameDir[7]'>Metal Gear Solid V: The Phantom Pain</a><br />");
                echo ("&nbsp &nbsp &nbsp<a href = './$dir[8]?game=$gameDir[8]'>Project Cars</a><br />");
                echo ("<a href = './$dir[17]'>Privacy</a><br />");
                echo ("<a href = './$dir[4]'>Shopping Cart</a><br />");
                echo ("<a href = './$dir[21]'>Sitemap</a><br />");

            ?>

        </div> <!--END SITE MAP-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once("./modules/end.php");
    // include_once("/home/eh1/e54061/public_html/wp/debug.php");

?>