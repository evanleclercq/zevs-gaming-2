<?php

    session_start();
    include_once("./modules/top.php");

    $_SESSION['page'] = "orderComplete.php";

?>

    <title>Order Complete</title>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

            <div id = "order">

                <?php

                    echo ("Your order has been placed successfully<br /><br />");

                    if (!isset($_SESSION['orderNo'])) {
                        $_SESSION['orderNo'] = rand(100000, 999999);
                    }
                    echo ("Your order number is: <span style = 'color:darkorange;font-weight:bold;'>" . $_SESSION['orderNo'] . "</span>");


                ?>
                    

            </div> <!--END ORDER-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once("./modules/end.php");
    // include_once("/home/eh1/e54061/public_html/wp/debug.php");

    $orderNo = $_SESSION['orderNo'];
    $user = $_SESSION['user']['email'];

    if (isset($_SESSION['cart'])) {

        // $fileOut = fopen ("./docs/order.txt", "r+");
        $fileOut = "./docs/order.txt";

        $output = "Account: " . $user . "\t" . "Order Number: " . $orderNo . "\r\n";

        file_put_contents($fileOut, $output);
        file_put_contents($fileOut, "------------------------------------------------------------------------------------\r\n", FILE_APPEND);

        // echo ("<pre>");
        // print_r($_SESSION['cart']);
        // echo ("</pre>");



        for ($i = 0; $i < count($_SESSION['cart']); $i++) {

            switch ($i) {

                case (0):
                    $prt = "witcher3";
                    break;
                case (1):
                    $prt = "batmanAK";
                    break;
                case (2):
                    $prt = "bfh";
                    break;
                case (3):
                    $prt = "banished";
                    break;
                case (4):
                    $prt = "cities";
                    break;
                case (5):
                    $prt = "metroLL";
                    break;
                case (6):
                    $prt = "mgsv";
                    break;
                case (7):
                    $prt = "projCars";
                    break;

            }

            if (isset($_SESSION['cart'][$prt]['prodID'])) {

                $prodID = $_SESSION['cart'][$prt]['prodID'];
                $prodTitle = $_SESSION['cart'][$prt]['prodName'];
                $prodCount = $_SESSION['cart'][$prt]['prodCount'];
                $prodCost = $_SESSION['cart'][$prt]['prodCost'];


                if ($prodCount >= 1) {

                    $output = "ID: " . $prodID . "\t" . "Title: " . $prodTitle . "\t" . "Quantity: " . $prodCount . "\t" . "Cost: $" . $prodCost . "\r\n";
                    file_put_contents($fileOut, $output, FILE_APPEND);
                }
            }

                


        }

        file_put_contents($fileOut, "------------------------------------------------------------------------------------\r\n", FILE_APPEND);

        $output = "Total Number of Items: " . $_SESSION['cart']['total']['totalCount'] . "\t" . "Total Cost: $" . $_SESSION['cart']['total']['totalCost'];
        file_put_contents($fileOut, $output, FILE_APPEND);

        // $order = array ($orderNo) => array ($_SESSION['cart']);

        // if (!isset($_COOKIE[$user])) {
        //     $_COOKIE[$user] = $order);
        // } else {
        //     array_push ($_COOKIE[$user], $order);
        // }

        // $_COOKIE[$user][$orderNo] = $_SESSION['cart'];
        unset($_SESSION['cart']);
        unset($_SESSION['orderNo']);
    }

    if (isset($_COOKIE[$user])) {
        echo ("<pre>");
        print_r($_COOKIE[$user]);
        echo ("</pre>");
    }


?>