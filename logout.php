<?php

    session_start();
    include_once ("./modules/top.php");

    $_SESSION['page'] = "logout.php";

?>

    <title>Logged Out</title>

    <meta http-equiv="refresh" content="6; url = index.php">

<?php

    session_unset();
    session_destroy();

    // include_once ("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

            <div id = "logout">

                <?php

                    echo ("<p>You Have been sucessfully Logged Out<br /></p>");
                    echo ("<p>You will be redirected shortly. If not click <a href = './index.php'>here</a></p>");

                ?>

            </div> <!--END LOGOUT-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once ("./modules/end.php");

?>