<?php

    session_start();
    include_once("./modules/top.php");

    $game = $_GET['game'];
    $_SESSION['page'] = "gameSingle.php?game=$game";
    // $id = $_GET[id];

    $title = file_get_contents("./games/$game/title.txt");
    $setprice = floatval (file_get_contents("./games/$game/price.txt"));
    $desc = file_get_contents("./games/$game/desc.txt");
    $box = "./games/$game/box.png";

    $discount = 0;

    if (!empty($_SESSION['user'])) {
        if (empty($_SESSION['cart'])) {
            $discount = $_SESSION['user']['discount1'] * 0.2;
        } else if ($_SESSION['cart']['total']['totalCount'] == 1) {
            $discount = $_SESSION['user']['discount2'] * 0.2;
        } else if ($_SESSION['cart']['total']['totalCount'] == 2) {
            $discount = $_SESSION['user']['discount3'] * 0.2;
        } else {
            $discount = 0; 
        }
    }

    $price = $setprice - ($setprice * $discount);




    echo ("<title>$title</title>");

?>

    <script type = "text/javascript" src = "./scripts/addToCart.js"></script>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

                    <div id = "productDetails">

                        <div id = "sidebar">

                            <?php

                                echo ("<img src = './games/$game/box.png' alt = 'Game Box Art'/>");

                                echo("<h2>$$price</h2>");

                             ?>

                            <div id = "addCart">

                                <?php echo ("<form action = './cart.php' method = 'post'>");?>
                                    <img id = "minus" src = "./images/cart/minus.png" alt = "Less Items" onClick = "updateAmount(-1)" width = 25 />
                                    <input id = "amount" type = "number" value = 1 name = "quantity">
                                    <img id = "plus" src = "./images/cart/plus.png" alt = "More Items" onClick = "updateAmount(1)"/>

                                    <div id = "hidden">

                                        <?php echo ("<input name = 'title' type = 'text' value = '$title'>")?>
                                        <?php echo ("<input name = 'id' type = 'text' value = '$game'>")?>
                                        <?php echo ("<input id = 'cost' name = 'cost' type = 'text' value = '$price'>") ?>


                                    </div> <!--END HIDDEN-->

                                         <?php echo ("<input id = 'price' name = 'price' type = 'number' value = $price>") ?>


                                    <input id = "addButton" type = "submit" value = "Add to Cart" name = "add">

                                </form>

                            </div> <!--END ADD CART-->

                        </div> <!--EnD SIDEBAR-->

                        <div id = "details">

                            <?php
                                echo ("<h2>$title</h2>");
                                echo ("<p>$desc</p>");
                            ?>
                        </div> <!--END DETAILS-->

                    </div> <!--END PRODUCT DETAILS-->

        </div><!--END BODY CONTENT-->

    </div> <!--END BODY-->


<?php

    include_once("./modules/end.php");
    // include_once("/home/eh1/e54061/public_html/wp/debug.php");


?>