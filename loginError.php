<?php

    session_start();
    include_once("./modules/top.php");

?>

    <title>Login</title>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

            <div id = "loginerror">

                <p>ERROR! Invalid Username or Password</p>

                <form action = "./modules/processLogin.php" method = "post">

                    <input type = "text" name = "username" placeholder = "username / email">
                    <br />
                    <input type = "password" name = "password" placeholder = "password">
                    <br />
                    <input id = "submit" type = "submit" value = "Log In">

                </form>

            </div> <!--END LOGIN ERROR-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once("./modules/end.php");
    // include_once("/home/eh1/e54061/public_html/wp/debug.php");

?>