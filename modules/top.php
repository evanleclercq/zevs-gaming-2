<!DOCTYPE html>

<html>

    <head>

        <link rel = "stylesheet" type = "text/css"  media =  "screen and (max-device-width: 740px)" href = "./css/styles_mobile.css"/>
        <link rel = "stylesheet" type = "text/css" media = "screen and (min-device-width: 737px)" href = "./css/styles.css"/>

        <link rel = "shortcut icon" href = "./images/ZEVS_ICON.ico"/>

        <meta charset="utf-8" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name = "viewport" content = "width = device-width, initial-scale=1" />