
            <div id = "footer">

                <ul>
                    <li> <a href = "./index.php">Home</a> </li>
                    <li> <a href = "./games.php">Games</a> </li>
                    <li> <a href = "./policy.php">Privacy</a> </li>
                    <li> <a href = "./contact.php">Contact Us</a> </li>
                    <li> <a href = "./sitemap.php">Site Map</a> </li>
                </ul>

                <!--The code used below in the footer was lifted directly from the sample landing page supplied by e54061-->
                <!--Code can be found at: http://titan.csit.rmit.edu.au/~e54061/wp/-->

                <div id = "validation">


                    <a href="http://validator.w3.org/check?uri=referer"><img style="border:0;height:25px" src="http://www.w3.org/html/logo/downloads/HTML5_Logo_32.png" alt="HTML Validator"></a>
                    <a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border:0; height:25px" src="http://jigsaw.w3.org/css-validator/images/vcss-blue" alt="CSS Validator"></a>

                </div> <!--END VALIDATION-->


                <p>
                    &copy; Evan le Clercq

                    <script>
                        document.write(new Date().getFullYear());
                    </script>

                </p>

            </div> <!--END FOOTER-->

        </div> <!--END PAGE WRAPPER-->

    </body>

</html>