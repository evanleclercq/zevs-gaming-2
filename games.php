<?php

    session_start();

    $_SESSION['page'] = "games.php";

    include_once("./modules/top.php");

    if (isset($_SESSION['message'])) {
        echo($_SESSION['message']);
        unset($_SESSION['message']);
    }

?>

    <title>Games</title>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

            <div id = "gameDisplay">

                <table>

                    <tr>

                        <td>
                            <h2>Witcher 3: Wild Hunt</h2>
                            <a href = "./gameSingle.php?game=witcher3"> <img src = "./images/box/witcher3.jpg" alt = "Witcher 3: Wild Hunt"/> </a>
                            <h3>$79.95</h3>

                            <form action = "./gameSingle.php?game=witcher3" method = "post">

                                <input type = "submit" name = "moreInfo" value = "More Information"/>

                            </form>
                        </td>
                        <td>
                            <h2>Batman Arkham Knight</h2>
                            <a href = "./gameSingle.php?game=batmanAK"> <img src = "./images/box/batmanAK.png" alt = "Batman Arkham Knight"/> </a>
                            <h3>$89.95</h3>

                            <form action = "./gameSingle.php?game=batmanAK" method = "post">

                                <input type = "submit" name = "moreInfo" value = "More Information"/>

                            </form>
                        </td>
                        <td>
                            <h2>Battlefield Hardline</h2>
                            <a href = "./gameSingle.php?game=bfh"> <img src = "./images/box/BFH.jpg" alt = "Battlefield Hardline"/> </a>
                            <h3>$59.95</h3>

                            <form action = "./gameSingle.php?game=bfh" method = "post">

                                <input type = "submit" name = "moreInfo" value = "More Information"/>

                            </form>
                        </td>

                    </tr>

                    <tr>

                        <td>
                            <h2>Banised</h2>
                            <a href = "./gameSingle.php?game=banished"> <img src = "./images/box/banished.jpg" alt = "Banised"/> </a>
                            <h3>$39.95</h3>

                            <form action = "./gameSingle.php?game=banished" method = "post">

                                <input type = "submit" name = "moreInfo" value = "More Information"/>

                            </form>
                        </td>
                        <td>
                            <h2>Cities: Skyline</h2>
                            <a href = "./gameSingle.php?game=cities"> <img src = "./images/box/cities.jpg" alt = "Cities Skyline"/> </a>
                            <h3>$59.95</h3>

                            <form action = "./gameSingle.php?game=cities" method = "post">

                                <input type = "submit" name = "moreInfo" value = "More Information"/>

                            </form>
                        </td>
                        <td>
                            <h2>Metro Last Light</h2>
                            <a href = "./gameSingle.php?game=metroLL"> <img src = "./images/box/metroLastLight.jpg" alt = "Metro Last Light"/> </a>
                            <h3>$49.95</h3>

                            <form action = "./gameSingle.php?game=metroLL" method = "post">

                                <input type = "submit" name = "moreInfo" value = "More Information"/>

                            </form>
                        </td>

                    </tr>

                    <tr>

                        <td>
                            <h2>Metal Gear Solid V: Phantom Pain</h2>
                            <a href = "./gameSingle.php?game=mgsv"> <img src = "./images/box/MGSV.jpg" alt = "Metal Gear Solid V: Phantom Pain"/> </a>
                            <h3>$99.95</h3>

                            <form action = "./gameSingle.php?game=mgsv" method = "post">

                                <input type = "submit" name = "moreInfo" value = "More Information"/>

                            </form>
                        </td>
                        <td>
                            <h2>Project Cars</h2>
                            <a href = "./gameSingle.php?game=projCars"> <img src = "./images/box/projectCars.jpg" alt = "Project Cars"/> </a>
                            <h3>$79.95</h3>

                            <form action = "./gameSingle.php?game=projCars" method = "post">

                                <input type = "submit" name = "moreInfo" value = "More Information"/>

                            </form>
                        </td>

                </table>

            </div> <!--END GAME DISPLAY-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once("./modules/end.php");
    // include_once("/home/eh1/e54061/public_html/wp/debug.php");

?>