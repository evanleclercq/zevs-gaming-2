It is the year 2034.
<br /><br />
Beneath the ruins of post-apocalyptic Moscow, in the tunnels of the Metro, the remnants of mankind are besieged by deadly threats from outside - and within.
<br /><br />
Mutants stalk the catacombs beneath the desolate surface, and hunt amidst the poisoned skies above.
<br /><br />
But rather than stand united, the station-cities of the Metro are locked in a struggle for the ultimate power, a doomsday device from the military vaults of D6. A civil war is stirring that could wipe humanity from the face of the earth forever.
<br /><br />
As Artyom, burdened by guilt but driven by hope, you hold the key to our survival - the last light in our darkest hour...
<br /><br /><br />
~ A gripping, story-driven first person shooter, Metro: Last Light is the hugely anticipated sequel to 2010's critically acclaimed classic Metro 2033
<br /><br />~ Experience thrilling combat with an exotic arsenal of hand-made weaponry against deadly foes - both human and mutant - and use stealth to launch attacks under the cover of darkness
<br /><br />~ Explore the post-apocalyptic world of the Moscow Metro, one of the most immersive, atmospheric game worlds ever created
<br /><br />~ Fight for every bullet and every last breath in a claustrophobic blend of survival horror and FPS gameplay
<br /><br />~ Next generation technology boasting stunning lighting and physics sets a new graphical benchmark on both console and PC
<br /><br />~ Wage post-apocalyptic warfare online, as Last Light delivers an intense multiplayer experience amongst the dark Russian ruins