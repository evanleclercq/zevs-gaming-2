<?php

    session_start();
    include_once("./modules/top.php");

    $_SESSION['page'] = "contact.php";

?>

    <title>Contact Us</title>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

        <!-- <img src = "../images/headings/contact.png" alt = "Contact Us" width = 250 /> -->

            <div id = "left">

                <div id = "contact">

                    <form action="./contact.php" method="post">

                        <div>

                            <label for="name">Name:</label>
                            <input type="text" id="name" />

                        </div>

                        <div>

                            <label for="mail">E-mail:</label>
                            <input type="email" id="mail" />

                        </div>

                        <div>

                            <label for="msg">Message:</label>
                            <textarea id="msg"></textarea>

                        </div>

                        <div class="button">

                            <input id = "submit" name = "send" type="submit" value = "Send Your Message" />

                        </div>

                    </form>

                </div> <!--END CONTACT-->

            </div> <!--END LEFT-->

            <div id = "right">

                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d52335.068622660925!2d138.6065112275081!3d-34.93299683902002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sau!4v1435637060759"
                allowfullscreen></iframe>

                <p>
                    ZEVS GAMING ONLINE <br/>
                    Adelaide <br/>
                    South Australia, 5000 <br/>
                </p>

            </div> <!--END RIGHT-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once("./modules/end.php");
    include_once("./modules/processContact.php");
    // include_once("/home/eh1/e54061/public_html/wp/debug.php");

?>