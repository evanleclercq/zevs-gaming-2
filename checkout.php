<?php

    session_start();
    include_once("./modules/top.php");

    $_SESSION['page'] = "checkout.php";

    //Will not let you proceed to checkout with nothing in cart

    if (!isset($_SESSION['cart'])) {
        header("location: ./cart.php");
        exit;
    }

?>

    <title>Checkout</title>

<?php

    // include_once("./modules/mid.php");

    if (empty($_SESSION['user'])) {

        include_once("./modules/loggedout.php");

    } else {

        include_once("./modules/loggedin.php");

    }

?>

    <div id = "body">

        <div id = "bodyContent">

            <div id = "checkout">

                <form action = "./review.php" method = "post">


                <?php

                    $fName = "";
                    $sName = "";
                    $email = "";
                    $phone = "";

                    if (!empty($_SESSION['user'])) {
                        $fName = $_SESSION['user']['fName'];
                        $sName = $_SESSION['user']['sName'];
                        $email = $_SESSION['user']['email'];
                        $phone = $_SESSION['user']['phone'];
                    } else {
                        $_SESSION['user'] = array ();
                    }

                ?>

                <div id = "userDetails">

                    <fieldset>
                    <legend>&nbsp Personal Information &nbsp</legend>
                    <br />

                    <?php



                        echo ("<label for='fname'>First Name: </label>");                    

                        if (!empty($fName)) {
                            echo ("<input required type = 'text' name = 'fname' value = $fName>");
                        } else {
                            echo ("<input required type = 'text' name = 'fname' placeholder = 'First Name'>");
                        }

                        echo ("<br /><br />");
                        echo ("<label for='sname'>Surname: </label>");

                        if (!empty($sName)) {
                            echo ("<input required type = 'text' name = 'sname' value = $sName>");
                        } else {
                            echo ("<input required type = 'text' name = 'sname' placeholder = 'Surname'>");
                        }

                        echo ("<br /><br />");
                        echo ("<label for='email'>Email Address: </label>");


                        if (!empty($email)) {
                            echo ("<input required type = 'email' name = 'email' value = $email>");
                        } else {
                            echo ("<input required type = 'email' name = 'email' placeholder = 'Email Address'>");
                        }

                        echo ("<br /><br />");
                        echo ("<label for='phone'>Phone Number: </label>");

                        if (!empty($phone)) {
                            echo ("<input required type = 'text' name = 'phone' value = $phone>");
                        } else {
                            echo ("<input required type = 'text' name = 'phone' placeholder = 'Contact Phone Number'>");
                        }

                        echo ("<br /><br />");

                        echo ("<label for='address'>Street Address: </label>");
                        echo ("<input required type = 'text' name = 'address' placeholder = 'Street Address'>");
                        echo ("<br /><br />");
                        echo ("<label for='suburb'>City / Suburb: </label>");
                        echo ("<input required type = 'text' name = 'suburb' placeholder = 'City / Suburb'>");
                        echo ("<br /><br />");
                        echo ("<label for='postcode'>Post Code: </label>");
                        echo ("<input required type = 'text' name = 'postcode' placeholder = 'Post Code'>");
                        echo ("<br /><br />");
                        echo ("<span style = 'font-weight:bold;''>Postage Method</span><br />");
                        echo ("| <input type = 'radio' name = 'postage' value = 'Standard' checked = 'checked'> Standard |\t");
                        echo ("<input type = 'radio' name = 'postage' value = 'Courier'> Courier |\t");
                        echo ("<input type = 'radio' name = 'postage' value = 'Express'> Express |\t");

                    ?>

                    </fieldset>

                </div> <!--END USER DETAILS-->

                <br />

                <div id = "paymentDetails">

                <fieldset>
                <legend>&nbsp Payment Information &nbsp</legend>
                <br />

                    

                        <label for='cardname'>Name on Card: </label>
                        <input required type = 'text' name = 'cardname' placeholder = 'Name on Card'>

                        <br /><br />

                        <label for='cardtype'>Card Type: </label>
                        <select required name = 'cardtype'>
                            <option selected value = ''>Please Select</option>
                            <option value = 'Visa'>Visa</option>
                            <option value = 'MasterCard'>Master Card</option>
                            <option value = 'Amex'>Amex</option>
                        </select>

                        <br /><br />

                        <label for='cardno'>Card No.: </label>
                        <input required type = 'text' name = 'cardno' placeholder = 'Card Number'>

                        <br /><br />

                        <label for='exp'>Expiry: </label>
                        
                        <select required name = 'month'>
                            <option selected value = ''>Month</option>
                            <option value = 'JAN'>JAN</option>
                            <option value = 'FEB'>FEB</option>
                            <option value = 'MAR'>MAR</option>
                            <option value = 'APR'>APR</option>
                            <option value = 'MAY'>MAY</option>
                            <option value = 'JUN'>JUN</option>
                            <option value = 'JUL'>JUL</option>
                            <option value = 'AUG'>AUG</option>
                            <option value = 'SEP'>SEP</option>
                            <option value = 'OCT'>OCT</option>
                            <option value = 'NOV'>NOV</option>
                            <option value = 'DEC'>DEC</option>
                        </select>

                        <select required name = 'year'>
                            <option selected value = ''>Year</option>
                            <option value = '2010'>2010</option>
                            <option value = '2011'>2011</option>
                            <option value = '2012'>2012</option>
                            <option value = '2013'>2013</option>
                            <option value = '2014'>2014</option>
                            <option value = '2015'>2015</option>
                            <option value = '2016'>2016</option>
                            <option value = '2017'>2017</option>
                            <option value = '2018'>2018</option>
                            <option value = '2019'>2019</option>
                            <option value = '2020'>2020</option>
                        </select>

                        <br /><br />

                        <label for='ccv'>CCV: </label>
                        <input required type = 'text' name = 'ccv' placeholder = 'CCV'>

                        <br /><br />

                

                    </fieldset>

                </div> <!--END PAYMENT DETAILS-->
                    <br />
                    <input type = 'checkbox' name = 'subscribe' value = 'subscribe'>  Subscribe to Newsletter
                    <br /><br />
                    <input id = 'submit' type = 'submit' name = 'finish' value = 'Review Order'>

                </form>

            </div> <!--END CHECKOUT-->

        </div> <!--END BODY CONTENT-->

    </div> <!--END BODY-->

<?php

    include_once("./modules/end.php");
    // include_once("/home/eh1/e54061/public_html/wp/debug.php");

?>